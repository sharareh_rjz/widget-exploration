package com.rjz.sharareh.widgetexploration;

import android.graphics.PorterDuff;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextClock;

import java.text.DateFormat;
import java.util.Date;
import java.util.TimeZone;

public class MainActivity extends AppCompatActivity {
    Button settext;
    EditText chtextbtn;
    CheckBox trans, tint, resize;
    RadioGroup city;
    Switch onoff;
    WebView google;
    TextClock time;
    ImageView imgview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        chtextbtn = (EditText) findViewById(R.id.editText_btntext);
        city = (RadioGroup) findViewById(R.id.radioGroup);
        time = (TextClock) findViewById(R.id.time);
        settext = (Button) findViewById(R.id.btn_hello);
        google = (WebView) findViewById(R.id.webView_google);
        //----------------------------------------------------------------------------
        city.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int check) {
                switch (check) {
                    case R.id.radio_london:
                        time.setText("5:25 PM");
                        break;
                    case R.id.radio_beijing:
                        time.setText("10:05 AM");
                        break;
                    case R.id.radio_newyork:
                        time.setText("2:06 PM");
                        break;
                }
            }
        });
        //-----------------------------------------------------------------------------
        onoff = (Switch) findViewById(R.id.switch1);
        onoff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    google.loadUrl("https://my.shatel.ir/");
            }
        });
        //-----------------------------------------------------------------------------
        settext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settext.setText(chtextbtn.getText());
            }
        });
        //----------------------------------------------------------------------------

    }
}
